﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public static class SuperSolver
    {
        public static double Calculate(double value1, double value2, string mathOperator)
        {
            double result = 0;


            switch(mathOperator)
            {
                case "/":
                    result = value1 / value2;
                    break;

                case "X":
                    result = value1 * value2;
                    break;
                     
                case "+":
                    result = value1 + value2;
                    break;

                case "-":
                    result = value1 - value2;
                    break;

            }


            //if (mathOperator == "+")
            //    result = value1 + value2;
            //else if (mathOperator == "-")
            //    result = value1 - value2;
            //else if (mathOperator == "x")
            //    result = value1 * value2;
            //else if (mathOperator == "/")
                //result = value1 / value2;


            return result;
        }
    }
}
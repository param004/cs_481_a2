﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Calculator
{
    public partial class MainPage : ContentPage
    {
        int FSA = 1;
        string SymbolMath;        
        double FirstN, secondN;
        string decimalpoint;

        public MainPage()
        {
            InitializeComponent();
            Clear(this, null);
        }

        void Hand_Click_Number(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            string pressed = button.Text;

            if (this.resultText.Text == "0" || FSA < 0)
            {
                resultText.Text = "";
                if (FSA < 0)
                    FSA *= -1;
            }

            if (this.resultText.Text == ".")
            {
               decimalpoint = pressed;
            }

            this.resultText.Text += pressed;

            double number;
            if (double.TryParse(resultText.Text, out number))
            {
                this.resultText.Text = number.ToString("N0");
                if (FSA == 1)
                {
                    FirstN = number;
                }
                else
                {
                    secondN = number;
                }
            }
        }

        //void SelectDecimal(object sender, EventArgs e)
        //{
        //    Button button = (Button)sender;
        //    string pressed = button.Text;
        //    decimalpoint = pressed;

        //    this.resultText.Text += pressed;



        //}

        void Hand_Click_Symbols(object sender, EventArgs e)
        {
            FSA = -2;
            Button button = (Button)sender;
            string pressed = button.Text;
            SymbolMath = pressed;
        }

        void Clear(object sender, EventArgs e)
        {
            FirstN = 0;
            secondN = 0;
            FSA = 1;
            resultText.Text = "0";
        }

        void Calculate(object sender, EventArgs e)
        {
            if (FSA == 2)
            {
                var result = SuperSolver.Calculate(FirstN, secondN, SymbolMath);

                this.resultText.Text = result.ToString();
                FirstN = result;
                FSA = -1;
            }
        }
    }
}